﻿using System;
using System.Collections.Generic;
using System.IO;
using VersionManager.Domain;
using VersionManager.Services.Implementations;

namespace VersionManager
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var parser = new ArgumentParser();
            var validator = new ArgumentValidator();
            var fileFinder = new AssemblyFileFinder();

            var tokens = parser.Parse(args);
            if (validator.Validate(tokens) == false)
            {
                Console.WriteLine("The incorrect tokens were passed in.");
                return;
            }

            string directory = tokens.Find(x => x.Type == ParameterType.Directory).Value;
            string setVersion = null;
            string incrementOption = null;

            if (tokens.Find(x => x.Type == ParameterType.Set) != null)
                setVersion = tokens.Find(x => x.Type == ParameterType.Set).Value;
            if (tokens.Find(x => x.Type == ParameterType.Increment) != null)
                incrementOption = tokens.Find(x => x.Type == ParameterType.Increment).Value;

            var files = fileFinder.FindFiles(directory);
            if (setVersion != null)
            {
                UpdateAssemblyFiles(setVersion, files);
            }
            else
            {
                var version = GetVersion(files[0], incrementOption);
                UpdateAssemblyFiles(version, files);
            }
        }

        private static string GetVersion(string fileName, string incParam)
        {
            var fileReader = new FileContentReader();
            var contents = fileReader.Read(fileName);
            var incrementer = new VersionIncrementer(new IncrementParameterValidator());
            return incrementer.Increment(contents, incParam);
        }

        private static void UpdateAssemblyFiles(string version, IEnumerable<string> files)
        {
            var fileUpdater = new AssemblyFileContentsUpdater();
            var fileReader = new FileContentReader();
            foreach (var file in files)
            {
                Console.WriteLine("Reading contents from: " + file);
                var contents = fileReader.Read(file);
                Console.WriteLine("Updating " + file + " assembly version to " + version);
                contents = fileUpdater.UpdateFileContents(contents, version);
                Console.WriteLine("Writing contents back to file.");
                using (var writer = new StreamWriter(file))
                {
                    writer.Write(contents);
                    writer.Close();
                }
            }
        }
    }
}
