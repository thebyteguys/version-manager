﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using VersionManager.Domain;
using VersionManager.Services.Implementations;

namespace VersionManager.Services.Tests.ArgumentValidatorTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_validating_arguments
    {
        private ArgumentValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new ArgumentValidator();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void And_the_arguments_list_is_null_then_an_exception_is_thrown()
        {
            _validator.Validate(null);
        }

        [Test]
        public void And_the_arguments_list_is_empty_then_false_is_returned()
        {
            var result = _validator.Validate(new List<Argument>());
            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_only_the_directory_then_false_is_returned()
        {
            var input = new List<Argument> {CreateDirectoryArgument()};
            var result = _validator.Validate(input);
            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_both_directory_and_set_then_true_is_returned()
        {
            var input = new List<Argument> {CreateDirectoryArgument(), CreateSetArgument()};
            var result = _validator.Validate(input);
            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_arguments_list_contains_only_set_then_false_is_returned()
        {
            var input = new List<Argument> {CreateSetArgument()};
            var result = _validator.Validate(input);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_only_increment_then_false_is_returned()
        {
            var input = new List<Argument> {CreateIncrementArgument("maj")};
            var result = _validator.Validate(input);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_both_set_and_increment_then_false_is_returned()
        {
            var input = new List<Argument> {CreateSetArgument(), CreateIncrementArgument("maj")};
            var result = _validator.Validate(input);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_directory_and_set_and_increment_then_false_is_returned()
        {
            var input = new List<Argument>
            {
                CreateDirectoryArgument(),
                CreateSetArgument(),
                CreateIncrementArgument("maj")
            };

            var result = _validator.Validate(input);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_two_directory_arguments_then_false_is_returned()
        {
            var input = new List<Argument> {CreateDirectoryArgument(), CreateDirectoryArgument()};
            var result = _validator.Validate(input);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_two_directory_arguments_and_a_set_argument_then_false_is_returned()
        {
            var input = new List<Argument> {CreateDirectoryArgument(), CreateDirectoryArgument(), CreateSetArgument()};
            var result = _validator.Validate(input);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_two_set_arguments_then_false_is_returned()
        {
            var input = new List<Argument> {CreateSetArgument(), CreateSetArgument()};
            var result = _validator.Validate(input);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_a_directory_argument_and_two_set_arguments_then_false_is_returned()
        {
            var input = new List<Argument> {CreateDirectoryArgument(), CreateSetArgument(), CreateSetArgument()};
            var result = _validator.Validate(input);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_two_increment_arguments_then_false_is_returned()
        {
            var input = new List<Argument> {CreateIncrementArgument("maj"), CreateIncrementArgument("maj")};
            var result = _validator.Validate(input);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_arguments_list_contains_a_directory_argument_and_two_increment_arguments_then_false_is_returned()
        {
            var input = new List<Argument>
            {
                CreateDirectoryArgument(),
                CreateIncrementArgument("maj"),
                CreateIncrementArgument("maj")
            };

            var result = _validator.Validate(input);

            Assert.IsFalse(result);
        }

        private Argument CreateDirectoryArgument()
        {
            return new Argument {Type = ParameterType.Directory, Value = "C:\\"};
        }

        private Argument CreateSetArgument()
        {
            return new Argument {Type = ParameterType.Set, Value = "2.0.1.1"};
        }

        private Argument CreateIncrementArgument(string option)
        {
            return new Argument {Type = ParameterType.Increment, Value = option};
        }
    }
    // ReSharper restore InconsistentNaming
}