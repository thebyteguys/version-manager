﻿using System;
using System.IO;
using NUnit.Framework;
using VersionManager.Services.Implementations;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Tests.FileContentReaderTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_reading_contents_from_a_file
    {
        private IFileContentReader _reader;

        public const string FileName = "file.txt";
        public const string FileContents =
            "[assembly: AssemblyVersion(\"1.0.0.0\")]\n[assembly: AssemblyFileVersion(\"1.0.0.0\")]";
        
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            CreateFile();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            File.Delete(FileName);
        }

        [SetUp]
        public void Setup()
        {
            _reader = new FileContentReader();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void And_the_file_name_is_null_then_an_exception_is_thrown()
        {
            _reader.Read(null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void And_the_file_name_is_empty_then_an_exception_is_thrown()
        {
            _reader.Read(string.Empty);
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void And_there_is_no_file_with_given_file_name_then_an_exception_is_thrown()
        {
            _reader.Read("madeupTextFile.txt");
        }

        [Test]
        public void And_the_file_exists_then_the_contents_are_read()
        {
            var expectedContents = FileContents;

            var acutalContents = _reader.Read("file.txt");

            Assert.AreEqual(expectedContents, acutalContents);
        }

        private void CreateFile()
        {
            using (var writer = new StreamWriter(FileName))
            {
                writer.Write(FileContents);
                writer.Close();
            }
        }
    }
    // ReSharper restore InconsistentNaming
}
