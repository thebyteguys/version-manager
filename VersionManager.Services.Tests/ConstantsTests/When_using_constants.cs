﻿using NUnit.Framework;

namespace VersionManager.Services.Tests.ConstantsTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_using_constants
    {
        [Test]
        public void Then_the_set_flag_matches_requirements()
        {
            Assert.AreEqual("-set:", Constants.SetFlag);
        }

        [Test]
        public void Then_the_inc_flag_matches_requirements()
        {
            Assert.AreEqual("-inc:", Constants.IncFlag);
        }

        [Test]
        public void Then_the_build_option_matches_requirements()
        {
            Assert.AreEqual("build", Constants.BuildOption);
        }

        [Test]
        public void Then_the_major_option_matches_requirements()
        {
            Assert.AreEqual("maj", Constants.MajorOption);
        }

        [Test]
        public void Then_the_minor_option_matches_requirements()
        {
            Assert.AreEqual("min", Constants.MinorOption);
        }

        [Test]
        public void Then_the_maintenance_option_matches_requirements()
        {
            Assert.AreEqual("main", Constants.MaintenanceOption);
        }
    }
    // ReSharper restore InconsistentNaming
}