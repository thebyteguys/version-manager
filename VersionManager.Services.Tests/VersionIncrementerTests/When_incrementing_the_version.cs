﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using VersionManager.Domain;
using VersionManager.Services.Implementations;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Tests.VersionIncrementerTests
{
    [TestFixture]
    public class When_incrementing_the_version
    {
        private IVersionIncrementer _incrementer;

        [SetUp]
        public void Setup()
        {
            _incrementer = new VersionIncrementer(new IncrementParameterValidator());
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void And_the_file_contents_is_null_then_an_exception_is_thrown()
        {
            _incrementer.Increment(null, Constants.MajorOption);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void And_the_file_contents_consists_of_whitespace_then_an_exception_is_thrown()
        {
            _incrementer.Increment(String.Empty, Constants.MajorOption);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void And_the_option_is_null_then_an_exception_is_thrown()
        {
            _incrementer.Increment("some valid string", null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void And_the_option_is_whitespace_then_an_exception_is_thrown()
        {
            _incrementer.Increment("some valid string", String.Empty);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void And_the_option_is_not_major_minor_maintenance_or_build_then_an_exception_is_thrown()
        {
            _incrementer.Increment("some valid string", "none valid option");
        }

        [Test]
        public void And_the_option_is_major_then_the_correct_version_is_returned()
        {
            var expectedVersion = "2.0.0.0";

            var contents = "[assembly: AssemblyVersion(\"1.0.0.0\")]";

            var actualVersion = _incrementer.Increment(contents, Constants.MajorOption);

            Assert.AreEqual(expectedVersion, actualVersion);
        }

        [Test]
        public void And_the_option_is_minor_then_the_correct_version_is_returned()
        {
            var expectedVersion = "1.1.0.0";

            var contents = "[assembly: AssemblyVersion(\"1.0.0.0\")]";

            var actualVersion = _incrementer.Increment(contents, Constants.MinorOption);

            Assert.AreEqual(expectedVersion, actualVersion);
        }

        [Test]
        public void And_the_option_is_maintenance_then_the_correct_version_is_returned()
        {
            var expectedVersion = "1.0.1.0";

            var contents = "[assembly: AssemblyVersion(\"1.0.0.0\")]";

            var actualVersion = _incrementer.Increment(contents, Constants.MaintenanceOption);

            Assert.AreEqual(expectedVersion, actualVersion);
        }

        [Test]
        public void And_the_option_is_build_then_the_correct_version_is_returned()
        {
            var expectedVersion = "1.0.0.1";

            var contents = "[assembly: AssemblyVersion(\"1.0.0.0\")]";

            var actualVersion = _incrementer.Increment(contents, Constants.BuildOption);

            Assert.AreEqual(expectedVersion, actualVersion);
        }

        [Test]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void And_the_option_is_build_but_that_piece_does_not_exist_then_an_exception_is_thrown()
        {
            var contents = "[assembly: AssemblyVersion(\"1.0.0\")]";

            _incrementer.Increment(contents, Constants.BuildOption);
        }

    }
}
