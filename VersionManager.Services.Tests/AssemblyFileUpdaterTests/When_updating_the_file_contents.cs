﻿using NUnit.Framework;
using System;
using VersionManager.Services.Implementations;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Tests.AssemblyFileUpdaterTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_updating_the_file_contents
    {
        private IFileContentsUpdater _updater;

        [SetUp]
        public void Setup()
        {
            _updater = new AssemblyFileContentsUpdater();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void And_the_file_contents_is_null_then_an_exception_is_thrown()
        {
            _updater.UpdateFileContents(null, "2.0.1");
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void And_the_version_is_null_then_an_exception_is_thrown()
        {
            _updater.UpdateFileContents("temp contents", null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void And_the_version_is_not_valid_then_an_exception_is_thrown()
        {
            _updater.UpdateFileContents("some contents", "a.2.3.4");
        }

        [Test]
        public void And_the_contents_do_not_contain_an_assembly_file_version_then_the_contents_do_not_change()
        {
            var result = _updater.UpdateFileContents("some contents", "2.0.1.1");

            Assert.AreEqual("some contents", result);
        }

        [Test]
        public void And_the_contents_contain_an_assembly_file_version_then_the_version_is_replaced()
        {
            const string expectedContents = "[assembly: AssemblyFileVersion(\"1.3.0.0\")]";

            var result = _updater.UpdateFileContents("[assembly: AssemblyFileVersion(\"0.0.0.0\")]", "1.3.0.0");

            Assert.AreEqual(expectedContents, result);
        }

        [Test]
        public void And_the_contents_contain_an_assembly_version_then_the_version_is_replaced()
        {
            const string expectedContents = "[assembly: AssemblyVersion(\"1.3.0.0\")]";

            var result = _updater.UpdateFileContents("[assembly: AssemblyVersion(\"0.0.0.0\")]", "1.3.0.0");

            Assert.AreEqual(expectedContents, result);
        }

        [Test]
        public void And_the_contents_contain_both_tokens_then_both_versions_are_replaced()
        {
            const string oldVersion = "1.0.0.0";
            const string newVersion = "1.3.0.0";
            const string contents = "[assembly: AssemblyFileVersion(\"{0}\")]\n" +
                                    "[assembly: AssemblyVersion(\"{0}\")]";
            
            var expectedContents = String.Format(contents, newVersion);

            var result = _updater.UpdateFileContents(String.Format(contents, oldVersion),
                newVersion);

            Assert.AreEqual(expectedContents, result);
        }
    }
    // ReSharper restore InconsistentNaming
}
