﻿using System;
using NUnit.Framework;
using VersionManager.Services.Implementations;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Tests.ParameterValidator.IncrementParameterValidatorTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_validating_the_increment_parameter
    {
        private IParameterValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new IncrementParameterValidator();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void And_the_option_is_null_then_an_exception_is_thrown()
        {
            _validator.Validate(null);
        }

        [Test]
        public void And_the_option_is_major_then_true_is_returned()
        {
            var result = _validator.Validate(Constants.MajorOption);

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_option_is_empty_then_false_is_returned()
        {
            var result = _validator.Validate(string.Empty);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_option_is_minor_then_true_is_returned()
        {
            var result = _validator.Validate(Constants.MinorOption);

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_option_is_maintenance_then_true_is_returned()
        {
            var result = _validator.Validate(Constants.BuildOption);

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_option_is_build_then_true_is_returned()
        {
            var result = _validator.Validate(Constants.BuildOption);

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_option_is_not_major_minor_maintenance_or_build_then_false_is_returned()
        {
            var result = _validator.Validate("invalid option");

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_option_is_uppercased_major_then_true_is_returned()
        {
            var result = _validator.Validate(Constants.MajorOption.ToUpper());

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_option_is_uppercased_minor_then_true_is_returned()
        {
            var result = _validator.Validate(Constants.MinorOption.ToUpper());

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_option_is_uppercased_maintenance_then_true_is_returned()
        {
            var result = _validator.Validate(Constants.MaintenanceOption.ToUpper());

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_option_is_uppercased_build_then_true_is_returned()
        {
            var result = _validator.Validate(Constants.BuildOption.ToUpper());

            Assert.IsTrue(result);
        }
    }
    // ReSharper restore InconsistentNaming
}
