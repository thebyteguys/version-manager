﻿using System;
using NUnit.Framework;
using VersionManager.Services.Implementations;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Tests.ParameterValidator.SetParameterValidatorTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_validating_the_set_parameter
    {
        private IParameterValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new SetParameterValidator();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void And_the_version_is_null_then_an_exception_is_thrown()
        {
            _validator.Validate(null);
        }

        [Test]
        public void And_the_version_is_empty_then_false_is_returned()
        {
            var result = _validator.Validate(String.Empty);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_version_contains_four_period_delimitted_numbers_then_true_is_returned()
        {
            var result = _validator.Validate("1.2.3.4");

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_version_contains_four_period_delimitted_letters_then_false_is_returned()
        {
            var result = _validator.Validate("a.b.c.d");

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_version_contains_more_than_four_period_delimitted_numbers_then_false_is_returned()
        {
            var result = _validator.Validate("1.2.3.5.6");

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_version_contains_a_letter_in_the_delimitted_list_then_false_is_returned()
        {
            var result = _validator.Validate("a.1.2.3");

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_version_contains_three_delimitted_numbers_then_true_is_returned()
        {
            var result = _validator.Validate("1.2.3");
            
            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_version_contains_two_delimitted_numbers_then_true_is_returned()
        {
            var result = _validator.Validate("1.2");

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_version_contains_just_a_number_then_true_is_returned()
        {
            var result = _validator.Validate("1");

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_version_number_is_delimitted_by_another_character_then_false_is_returned()
        {
            var result = _validator.Validate("1,2,3,4");

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_version_number_is_greater_than_the_maximum_then_false_is_returned()
        {
            var result = _validator.Validate((SetParameterValidator.MaxVersionNumber + 1).ToString());
            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_version_number_is_the_maximum_allowed_then_true_is_returned()
        {
            var result = _validator.Validate(SetParameterValidator.MaxVersionNumber.ToString());
        }
    }
    // ReSharper restore InconsistentNaming
}
