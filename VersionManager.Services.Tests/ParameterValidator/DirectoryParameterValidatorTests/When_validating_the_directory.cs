﻿using System;
using NUnit.Framework;
using VersionManager.Services.Implementations;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Tests.ParameterValidator.DirectoryParameterValidatorTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_validating_the_directory
    {
        private IParameterValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new DirectoryParameterValidator();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void And_the_directory_is_null_then_an_exception_is_thrown()
        {
            _validator.Validate(null);
        }

        [Test]
        public void And_the_directory_is_empty_then_false_is_returned()
        {
            var result = _validator.Validate(String.Empty);

            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_directory_exists_then_true_is_returned()
        {
            var existingDirectory = "C:\\";

            var result = _validator.Validate(existingDirectory);

            Assert.IsTrue(result);
        }

        [Test]
        public void And_the_directory_does_not_exist_then_false_is_returned()
        {
            var wrongDirectory = "wrongStuff";

            var result = _validator.Validate(wrongDirectory);

            Assert.IsFalse(result);
        }
    }
    // ReSharper restore InconsistentNaming
}
