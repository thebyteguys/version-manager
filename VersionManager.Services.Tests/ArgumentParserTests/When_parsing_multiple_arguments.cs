﻿using NUnit.Framework;
using VersionManager.Domain;
using VersionManager.Services.Implementations;

namespace VersionManager.Services.Tests.ArgumentParserTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_parsing_multiple_arguments
    {
        private ArgumentParser _parser;

        [SetUp]
        public void Setup()
        {
            _parser = new ArgumentParser();
        }

        [Test]
        public void And_the_directory_option_and_the_set_options_are_passed_then_the_correct_arguments_are_returned()
        {
            var expectedDirectoryOption = new Argument {Type = ParameterType.Directory, Value = "directory"};
            var expectedSetOption = new Argument {Type = ParameterType.Set, Value = "2.0.0.1"};
            var expectedResults = new[] {expectedDirectoryOption, expectedSetOption};
            
            var arguments = new[] {"directory", "-set:2.0.0.1"};

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResults, results);
        }

        [Test]
        public void And_the_directory_option_and_the_inc_options_with_major_are_passed_then_the_correct_arguments_are_returned()
        {
            var expectedDirectoryOption = new Argument { Type = ParameterType.Directory, Value = "directory" };
            var expectedIncOption = new Argument { Type = ParameterType.Increment, Value = "maj" };
            var expectedResults = new[] { expectedDirectoryOption, expectedIncOption };

            var arguments = new[] { "directory", "-inc:maj" };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResults, results);
        }

        [Test]
        public void And_the_directory_option_and_the_inc_options_with_minor_are_passed_then_the_correct_arguments_are_returned()
        {
            var expectedDirectoryOption = new Argument { Type = ParameterType.Directory, Value = "directory" };
            var expectedIncOption = new Argument { Type = ParameterType.Increment, Value = "min" };
            var expectedResults = new[] { expectedDirectoryOption, expectedIncOption };

            var arguments = new[] { "directory", "-inc:min" };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResults, results);
        }

        [Test]
        public void And_the_directory_option_and_the_inc_options_with_maintenance_are_passed_then_the_correct_arguments_are_returned()
        {
            var expectedDirectoryOption = new Argument { Type = ParameterType.Directory, Value = "directory" };
            var expectedIncOption = new Argument { Type = ParameterType.Increment, Value = "main" };
            var expectedResults = new[] { expectedDirectoryOption, expectedIncOption };

            var arguments = new[] { "directory", "-inc:main" };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResults, results);
        }

        [Test]
        public void And_the_directory_option_and_the_inc_options_with_build_are_passed_then_the_correct_arguments_are_returned()
        {
            var expectedDirectoryOption = new Argument { Type = ParameterType.Directory, Value = "directory" };
            var expectedIncOption = new Argument { Type = ParameterType.Increment, Value = "build" };
            var expectedResults = new[] { expectedDirectoryOption, expectedIncOption };

            var arguments = new[] { "directory", "-inc:build" };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResults, results);
        }
    }
    // ReSharper restore InconsistentNaming
}
