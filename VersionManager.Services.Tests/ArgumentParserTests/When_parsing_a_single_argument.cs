﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using VersionManager.Domain;
using VersionManager.Services.Implementations;

namespace VersionManager.Services.Tests.ArgumentParserTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_parsing_a_single_argument
    {
        private ArgumentParser _parser;

        [SetUp]
        public void Setup()
        {
            _parser = new ArgumentParser();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void And_the_arguments_is_null_then_an_exception_is_thrown()
        {
            _parser.Parse(null);
        }

        [Test]
        public void And_the_arguments_is_empty_then_an_empty_list_of_arguments_is_returned()
        {
            var results = _parser.Parse(new string[]{});

            CollectionAssert.IsEmpty(results);
        }

        [Test]
        public void And_the_arguments_include_the_directory_then_the_directory_argument_is_returned()
        {
            var expectedResult = new List<Argument>{new Argument {Type = ParameterType.Directory, Value = "something"}};
            var arguments = new [] {"something"};

            var result = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void And_the_arguments_include_the_set_flag_then_the_correct_argument_is_returned()
        {
            var expectedResult = new List<Argument>
            {
                new Argument {Type = ParameterType.Set, Value = "2.0.1.1"}
            };

            var arguments = new[] {"-set:2.0.1.1"};

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, results);
        }

        [Test]
        public void And_the_arguments_include_the_inc_flag_with_major_then_the_correct_argument_is_returned()
        {
            var expectedResult = new List<Argument>
            {
                new Argument {Type = ParameterType.Increment, Value = "maj"}
            };

            var arguments = new[] { "-inc:maj" };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, results);
        }

        [Test]
        public void And_the_arguments_include_the_inc_flag_with_minor_then_the_correct_argument_is_returned()
        {
            var expectedResult = new List<Argument>
            {
                new Argument {Type = ParameterType.Increment, Value = "min"}
            };

            var arguments = new[] { "-inc:min" };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, results);
        }
        
        [Test]
        public void And_the_arguments_include_the_inc_flag_with_maintenance_then_the_correct_argument_is_returned()
        {
            var expectedResult = new List<Argument>
            {
                new Argument {Type = ParameterType.Increment, Value = "main"}
            };

            var arguments = new[] { "-inc:main" };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, results);
        }

        [Test]
        public void And_the_arguments_include_the_inc_flag_with_build_then_the_correct_argument_is_returned()
        {
            var expectedResult = new List<Argument>
            {
                new Argument {Type = ParameterType.Increment, Value = "build"}
            };

            var arguments = new[] { "-inc:build" };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, results);
        }

        #region Case-Insensitive Tests

        [Test]
        public void And_the_arguments_include_the_upper_case_set_flag_then_the_correct_argument_is_returned()
        {
            var expectedResult = new List<Argument>
            {
                new Argument {Type = ParameterType.Set, Value = "2.0.1.1"}
            };

            var arguments = new[] {Constants.SetFlag.ToUpper() + "2.0.1.1" };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, results);
        }

        [Test]
        public void And_the_arguments_include_the_upper_case_inc_flag_with_major_then_the_correct_argument_is_returned()
        {
            var expectedResult = new List<Argument>
            {
                new Argument {Type = ParameterType.Increment, Value = "maj"}
            };

            var arguments = new[] { Constants.IncFlag.ToUpper() + Constants.MajorOption };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, results);
        }

        [Test]
        public void And_the_arguments_include_the_upper_case_inc_flag_with_minor_then_the_correct_argument_is_returned()
        {
            var expectedResult = new List<Argument>
            {
                new Argument {Type = ParameterType.Increment, Value = "min"}
            };

            var arguments = new[] { Constants.IncFlag.ToUpper() + Constants.MinorOption };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, results);
        }

        [Test]
        public void And_the_arguments_include_the_upper_case_inc_flag_with_maintenance_then_the_correct_argument_is_returned()
        {
            var expectedResult = new List<Argument>
            {
                new Argument {Type = ParameterType.Increment, Value = "main"}
            };

            var arguments = new[] { Constants.IncFlag.ToUpper() + Constants.MaintenanceOption };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, results);
        }

        [Test]
        public void And_the_arguments_include_the_upper_case_inc_flag_with_build_then_the_correct_argument_is_returned()
        {
            var expectedResult = new List<Argument>
            {
                new Argument {Type = ParameterType.Increment, Value = "build"}
            };

            var arguments = new[] { Constants.IncFlag.ToUpper() + Constants.BuildOption };

            var results = _parser.Parse(arguments);

            Assert.AreEqual(expectedResult, results);
        }

        #endregion
    }
    // ReSharper restore InconsistentNaming
}
