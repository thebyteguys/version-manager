﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using VersionManager.Services.Implementations;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Tests.AssemblyFileFinderTests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_finding_files
    {
        private IFileFinder _finder;
        private bool _shouldCleanup;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            _shouldCleanup = false;
            if (Directory.Exists(@"C:\otherTemp\") == false)
            {
                Directory.CreateDirectory(@"C:\otherTemp\");
                _shouldCleanup = true;
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            if (_shouldCleanup)
                Directory.Delete(@"C:\otherTemp\", recursive:true);
        }

        [SetUp]
        public void Setup()
        {
            _finder = new AssemblyFileFinder();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void And_the_directory_is_null_then_an_exception_is_thrown()
        {
            _finder.FindFiles(null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void And_the_directory_does_not_exist_then_an_exception_is_thrown()
        {
            _finder.FindFiles("some bad directory");
        }

        [Test]
        public void And_the_directory_exists_but_no_files_are_found_then_an_empty_list_is_returned()
        {
            var files = _finder.FindFiles(@"C:\otherTemp");

            Assert.IsEmpty(files);
        }

        [Test]
        public void And_the_directory_exists_and_there_are_assembly_info_files_then_the_list_of_file_names_is_returned()
        {
            CreateFiles();

            var expectedResults = new List<string> {@"C:\otherTemp\AssemblyInfo.cs"};

            var files = _finder.FindFiles(@"C:\otherTemp");

            CollectionAssert.AreEquivalent(expectedResults, files);

            DeleteFiles();
        }

        private void CreateFiles()
        {
            File.Create(@"C:\otherTemp\AssemblyInfo.cs").Close();
        }

        private void DeleteFiles()
        {
            File.Delete(@"C:\otherTemp\AssemblyInfo.cs");
        }
    }
    // ReSharper restore InconsistentNaming
}
