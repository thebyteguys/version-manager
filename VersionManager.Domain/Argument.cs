﻿using System;

namespace VersionManager.Domain
{
    public class Argument
    {
        public String Value { get; set; }

        public ParameterType Type { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            return obj.GetType() == GetType() && Equals((Argument) obj);
        }

        protected bool Equals(Argument other)
        {
            return string.Equals(Value, other.Value) && Type == other.Type;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Value != null ? Value.GetHashCode() : 0) * 397) ^ (int)Type;
            }
        }

        public override string ToString()
        {
            return "Type: " + Type + "\nValue: " + Value;
        }
    }
}
