﻿namespace VersionManager.Domain
{
    public enum ParameterType
    {
        Directory,
        Increment,
        Set
    }
}
