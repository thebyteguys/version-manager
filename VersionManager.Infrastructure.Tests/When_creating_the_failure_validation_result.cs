﻿using System;
using NUnit.Framework;

namespace VersionManager.Infrastructure.Tests
{
    // ReSharper disable InconsistentNaming
    [TestFixture]
    public class When_creating_the_failure_validation_result
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void And_the_message_is_null_then_an_exception_is_thrown()
        {
            ValidationResult.CreateFailureResult(null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void And_the_message_is_empty_then_an_exception_is_thrown()
        {
            ValidationResult.CreateFailureResult("");
        }
        
        [Test]
        public void Then_the_was_successful_flag_is_set_to_false()
        {
            var result = ValidationResult.CreateFailureResult("ignored").WasSuccessful;
            Assert.IsFalse(result);
        }

        [Test]
        public void And_the_message_is_valid_then_the_result_has_the_correct_message_set()
        {
            const string expectedMessage = "message";

            var result = ValidationResult.CreateFailureResult(expectedMessage).Message;

            Assert.AreEqual(expectedMessage, result);
        }
    }
    // ReSharper restore InconsistentNaming
}
