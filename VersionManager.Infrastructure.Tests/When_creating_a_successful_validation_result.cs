﻿using System;
using NUnit.Framework;

namespace VersionManager.Infrastructure.Tests
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class When_creating_a_successful_validation_result
    {
        [Test]
        public void Then_was_successful_is_true()
        {
            var result = ValidationResult.CreateSuccessfulResult().WasSuccessful;

            Assert.IsTrue(result);
        }

        [Test]
        public void Then_the_message_is_empty()
        {
            var result = ValidationResult.CreateSuccessfulResult().Message;

            Assert.AreEqual(String.Empty, result);
        }
    }
// ReSharper restore InconsistentNaming
}
