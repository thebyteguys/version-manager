﻿using System;

namespace VersionManager.Infrastructure
{
    public class ValidationResult
    {
        public bool WasSuccessful { get; private set; }

        public string Message { get; private set; }

        private ValidationResult(bool wasSuccessful, string message)
        {
            WasSuccessful = wasSuccessful;
            Message = message;
        }

        public static ValidationResult CreateSuccessfulResult()
        {
            return new ValidationResult(true, String.Empty);
        }

        public static ValidationResult CreateFailureResult(string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            if (String.IsNullOrWhiteSpace(message))
                throw new ArgumentException("Message cannot consist of only whitespace.");

            return new ValidationResult(false, message);
        }
    }
}
