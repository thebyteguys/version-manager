﻿namespace VersionManager
{
    public static class Constants
    {
        public static string SetFlag = "-set:";
        public static string IncFlag = "-inc:";
        public static string BuildOption = "build";
        public static string MajorOption = "maj";
        public static string MinorOption = "min";
        public static string MaintenanceOption = "main";
    }
}
