﻿namespace VersionManager.Services.Interfaces
{
    public interface IParameterValidator
    {
        bool Validate(string value);
    }
}
