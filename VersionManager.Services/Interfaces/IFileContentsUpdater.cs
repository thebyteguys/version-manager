﻿namespace VersionManager.Services.Interfaces
{
    public interface IFileContentsUpdater
    {
        string UpdateFileContents(string contents, string version);
    }
}
