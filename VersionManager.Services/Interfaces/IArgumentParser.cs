﻿using System.Collections.Generic;
using VersionManager.Domain;

namespace VersionManager.Services.Interfaces
{
    public interface IArgumentParser
    {
        List<Argument> Parse(string[] input);
    }
}
