﻿using System.Collections.Generic;
using VersionManager.Domain;

namespace VersionManager.Services.Interfaces
{
    public interface IArgumentValidator
    {
        bool Validate(List<Argument> arguments);
    }
}
