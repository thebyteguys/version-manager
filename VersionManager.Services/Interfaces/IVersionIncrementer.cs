﻿namespace VersionManager.Services.Interfaces
{
    public interface IVersionIncrementer
    {
        string Increment(string contents, string option);
    }
}
