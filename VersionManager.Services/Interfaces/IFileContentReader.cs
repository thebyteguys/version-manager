﻿namespace VersionManager.Services.Interfaces
{
    public interface IFileContentReader
    {
        string Read(string fileName);
    }
}
