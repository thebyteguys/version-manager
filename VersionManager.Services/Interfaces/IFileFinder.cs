﻿using System.Collections.Generic;

namespace VersionManager.Services.Interfaces
{
    public interface IFileFinder
    {
        List<string> FindFiles(string directory);
    }
}
