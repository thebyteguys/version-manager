﻿using System;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Implementations
{
    public class AssemblyFileContentsUpdater : IFileContentsUpdater
    {
        public const string AssemblyFileToken = "AssemblyFileVersion(\"";
        public const string AssemblyToken = "AssemblyVersion(\"";

        public string UpdateFileContents(string fileContents, string version)
        {
            if (fileContents == null)
                throw new ArgumentNullException("fileContents");
            if (version == null)
                throw new ArgumentNullException("version");

            var validator = new SetParameterValidator();
            if (validator.Validate(version) == false)
                throw new ArgumentException("The version is not valid: " + version);

            fileContents = UpdateAssemblyFileVersion(fileContents, version);
            fileContents = UpdateAssemblyVersion(fileContents, version);

            return fileContents;
        }

        private string UpdateAssemblyFileVersion(string fileContents, string version)
        {
            return ReplaceToken(fileContents, version, AssemblyFileToken);
        }

        private string UpdateAssemblyVersion(string fileContents, string version)
        {
            return ReplaceToken(fileContents, version, AssemblyToken);
        }

        private string ReplaceToken(string fileContents, string version, string token)
        {
            if (!fileContents.Contains(token))
                return fileContents;

            var startIndex = fileContents.LastIndexOf(token, StringComparison.Ordinal) + token.Length;
            var endIndex = fileContents.IndexOf('"', startIndex);

            fileContents = fileContents.Remove(startIndex, endIndex - startIndex);

            return fileContents.Insert(startIndex, version);
        }
    }
}
