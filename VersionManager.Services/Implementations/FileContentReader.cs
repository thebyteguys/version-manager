﻿using System;
using System.IO;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Implementations
{
    public class FileContentReader : IFileContentReader
    {
        public string Read(string fileName)
        {
            if (fileName == null)
                throw new ArgumentNullException("fileName");
            if (String.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("fileName cannot consist of whitespace.");
            if (!File.Exists(fileName))
                throw new FileNotFoundException("Could not find file: " + fileName);

            string contents;
            using (var reader = new StreamReader(fileName))
            {
                contents = reader.ReadToEnd();
                reader.Close();
            }
            return contents;
        }
    }
}
