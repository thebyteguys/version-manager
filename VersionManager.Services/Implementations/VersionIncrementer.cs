﻿using System;
using System.Globalization;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Implementations
{
    public class VersionIncrementer : IVersionIncrementer
    {
        private readonly IParameterValidator _incrementValidator;

        public VersionIncrementer(IParameterValidator validator)
        {
            _incrementValidator = validator;
        }
        public string Increment(string contents, string option)
        {
            if (contents == null)
                throw new ArgumentNullException("contents");
            if (String.IsNullOrWhiteSpace(contents))
                throw new ArgumentException("fileName cannot consist of whitespace");
            if (!_incrementValidator.Validate(option))
                throw new ArgumentException(option + " is not a valid increment option");

            var oldVersion = GetVersion(contents);
            var versionPieces = oldVersion.Split('.');
            int index;
            if (option == Constants.MajorOption)
                index = 0;
            else if (option == Constants.MinorOption)
                index = 1;
            else if (option == Constants.MaintenanceOption)
                index = 2;
            else
                index = 3;

            if (index >= versionPieces.Length)
                throw new IndexOutOfRangeException("Cannot access index " + index + " in version array of " + versionPieces.Length + "pieces");
            versionPieces[index] = IncrementPiece(versionPieces[index]);
            return String.Join(".", versionPieces);
        }

        private string IncrementPiece(string versionPiece)
        {
            if (versionPiece == "*")
            {
                throw new ArgumentException("Cannot increment version piece consisting of *");
            }
            int version;
            if (!Int32.TryParse(versionPiece, out version))
                throw new ArgumentException("Could not convert " + versionPiece + " to a number.");

            return (version + 1).ToString(CultureInfo.InvariantCulture);
        }

        private string GetVersion(string contents)
        {
            const string part = "AssemblyVersion(\"";
            var startPos = contents.IndexOf(part, StringComparison.Ordinal);
            startPos += part.Length;
            var endPos = contents.IndexOf('"', startPos);
            return contents.Substring(startPos, endPos - startPos);
        }
    }
}
