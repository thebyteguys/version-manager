﻿using System;
using System.Collections.Generic;
using VersionManager.Domain;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Implementations
{
    public class ArgumentParser : IArgumentParser
    {
        public List<Argument> Parse(string[] input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            var results = new List<Argument>();
            foreach (var token in input)
            {
                var loweredToken = token.ToLower();
                if (loweredToken.StartsWith(Constants.SetFlag))
                    results.Add(CreateSetArgument(token));
                else if (loweredToken.StartsWith(Constants.IncFlag))
                    results.Add(CreateIncrementArgument(token));
                else
                    results.Add(CreateDirectoryArgument(token));
            }
            return results;
        }

        private static Argument CreateSetArgument(string value)
        {
            var version = value.Substring(value.IndexOf(':') + 1);
            return CreateArgument(ParameterType.Set, version);
        }

        private static Argument CreateIncrementArgument(string value)
        {
            var option = value.Substring(value.IndexOf(':') + 1);
            return CreateArgument(ParameterType.Increment, option);
        }

        private static Argument CreateDirectoryArgument(string value)
        {
            return CreateArgument(ParameterType.Directory, value);
        }

        private static Argument CreateArgument(ParameterType type, string value)
        {
            return new Argument {Type = type, Value = value};
        }
    }
}
