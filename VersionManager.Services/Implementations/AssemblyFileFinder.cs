﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Implementations
{
    public class AssemblyFileFinder : IFileFinder
    {
        public List<string> FindFiles(string directory)
        {
            if (directory == null)
                throw new ArgumentNullException("directory");
            if (!Directory.Exists(directory))
                throw new ArgumentException("The directory: " + directory + " does not exist.");

            return Directory.GetFiles(directory, "AssemblyInfo.cs", SearchOption.AllDirectories).ToList();
        }
    }
}
