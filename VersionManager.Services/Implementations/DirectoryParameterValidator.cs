﻿using System;
using System.IO;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Implementations
{
    public class DirectoryParameterValidator : IParameterValidator
    {
        public bool Validate(string directory)
        {
            if (directory == null)
                throw new ArgumentNullException("directory");
            if (String.IsNullOrWhiteSpace(directory))
                return false;

            return Directory.Exists(directory);
        }
    }
}
