﻿using System;
using System.Linq;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Implementations
{
    public class SetParameterValidator : IParameterValidator
    {
        public const int MaxVersionNumber = 65534;

        public bool Validate(string version)
        {
            if (version == null)
                throw new ArgumentNullException("version");

            var versionPieces = version.Split('.');
            
            if (versionPieces.Count() > 4)
                return false;
            
            foreach (var piece in versionPieces)
            {
                int outParameter;
                if (!Int32.TryParse(piece, out outParameter))
                    return false;
                if (outParameter > MaxVersionNumber)
                    return false;
            }
            return true;
        }
    }
}
