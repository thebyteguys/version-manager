﻿using System;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Implementations
{
    public class IncrementParameterValidator : IParameterValidator
    {
        public bool Validate(string option)
        {
            if (option == null)
                throw new ArgumentNullException("option");
            if (String.IsNullOrWhiteSpace(option))
                return false;

            var loweredOption = option.ToLower();
            if (loweredOption != Constants.MajorOption && 
                loweredOption != Constants.MinorOption &&
                loweredOption != Constants.MaintenanceOption && 
                loweredOption != Constants.BuildOption)
                return false;
            return true;
        }
    }
}
