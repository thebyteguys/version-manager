﻿using System;
using System.Collections.Generic;
using System.Linq;
using VersionManager.Domain;
using VersionManager.Services.Interfaces;

namespace VersionManager.Services.Implementations
{
    public class ArgumentValidator : IArgumentValidator
    {
        public bool Validate(List<Argument> arguments)
        {
            if (arguments == null)
                throw new ArgumentNullException("arguments");

            var isDirectoryValid = IsDirectoryValid(arguments);
            var doesSetExist = IsSetValid(arguments);
            var doesIncrementExist = IsIncrementValid(arguments);

            if (doesSetExist && doesIncrementExist)
                return false;

            if (isDirectoryValid && doesSetExist)
                return true;

            if (isDirectoryValid && doesIncrementExist)
                return true;

            return false;
        }

        private bool DoesArgumentsContainTheRightNumberOfParameterType(List<Argument> arguments, ParameterType type)
        {
            if (arguments.Exists(x => x.Type == type) == false)
                return false;
            if (arguments.Count(x => x.Type == type) > 1)
                return false;
            return true;
        }

        private bool IsDirectoryValid(List<Argument> arguments)
        {
            if (!DoesArgumentsContainTheRightNumberOfParameterType(arguments, ParameterType.Directory))
                return false;

            var directory = arguments.Find(x => x.Type == ParameterType.Directory).Value;
         
            var validator = new DirectoryParameterValidator();
            return validator.Validate(directory);
        }

        private bool IsSetValid(List<Argument> arguments)
        {
            if (!DoesArgumentsContainTheRightNumberOfParameterType(arguments, ParameterType.Set))
                return false;

            var version = arguments.Find(x => x.Type == ParameterType.Set).Value;

            var validator = new SetParameterValidator();

            return validator.Validate(version);
        }

        private bool IsIncrementValid(List<Argument> arguments)
        {
            if (!DoesArgumentsContainTheRightNumberOfParameterType(arguments, ParameterType.Increment))
                return false;

            var incrementOption = arguments.Find(x => x.Type == ParameterType.Increment).Value;
            var validator = new IncrementParameterValidator();
            return validator.Validate(incrementOption);
        }
    }
}
